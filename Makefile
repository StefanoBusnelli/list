GCC=gcc
CFLAGS= -ggdb -Wall
CC=$(GCC) $(CFLAGS)

LIBNAME=list
LDNAME=lib$(LIBNAME).so
STNAME=lib$(LIBNAME).a
SONAME=$(LDNAME).1
REALNAME=$(SONAME).3.0

deps=$(REALNAME)

all: libs

.PHONY: libs doc test clean install

################################################################################
#
#	Shared library
#

libs: list.o $(deps)

list.o: list.c list.h
	$(CC) -c -fPIC -o $@ $< -I.
	ar rcs $(STNAME) $@

$(REALNAME): list.o
	$(CC) -shared -fPIC -Wl,-soname,$(SONAME) -o $@ $^ -lc
	ln -f -s $(REALNAME) $(SONAME)
	ln -f -s $(REALNAME) $(LDNAME)

################################################################################
#
#	Test
#

test: $(deps)
	$(MAKE) --directory=$@

################################################################################
#
#	Clean
#

clean: list.h list.c
	rm -f $(STNAME) $(LDNAME) $(LDNAME).* *.o
	$(MAKE) clean --directory=test

################################################################################
#
#	Install
#

install:
	if [ ! -d $(HOME)/include ]; then mkdir $(HOME)/include; fi
	if [ ! -d $(HOME)/lib ];     then mkdir $(HOME)/lib;     fi
	cp -v   list.h       $(HOME)/include/
	cp -v   $(STNAME)    $(HOME)/lib/
	cp -v   $(REALNAME)  $(HOME)/lib/
	cp -vlf $(LDNAME)    $(HOME)/lib/
	cp -vlf $(SONAME)    $(HOME)/lib/

################################################################################
#
#	Doxygen
#

doc:
	doxygen Doxyfile
