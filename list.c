/**
 *
 *  @file       list.c
 *  @brief      Liste
 *  @author     Stefano Busnelli busnelli.stefano@gmail.com
 *  @version    1.3.0
 *
 */

#include "list.h"

//	Node

/** @brief      Creazione di un nodo
 *  @details    Riserva la memoria per la struttura dati e restituisce il puntatore al nodo creato
 *              Inizializza a NULL i puntatori contenuti nel nodo
 *  @param      **n Indirizzo del puntatore del nodo da creare
 *  @return     t_node **
 *  @code
 *  t_node *n = NULL;
 *  node_create( &n );
 *  @endcode
 */
void node_create( t_node **n ) {
  *n = ( t_node * ) calloc( 1, sizeof( t_node ) );
  if ( *n != NULL ) {
    (( t_node * )(*n))->data = NULL;
    (( t_node * )(*n))->next = NULL;
    (( t_node * )(*n))->prev = NULL;
  }
}

/** @brief      Rimozione di un nodo dalla lista
 *  @details    Rimuove il nodo dalla lista collegando tra loro i nodi precedente e successivo, se sono presenti
 *  @param      *n Puntatore del nodo da creare
 *  @code
 *  t_node *n = NULL;
 *  node_create( &n );
 *  node_unlink( n );
 *  @endcode
 */
void node_unlink( t_node *n ) {
  /*	m - n - o
            n
            m - o
  */
  if ( n != NULL ) {
    // Collego tra loro eventuali nodi collegati
    if ( n->prev != NULL )
      n->prev->next = n->next;
    if ( n->next != NULL )
      n->next->prev = n->prev;
  }
}

/** @brief      Concatenazione di nodi
 *  @details    Collega il nodo n come predecessore di m che può essere un nodo o una concatenazione di nodi
 *  @param      *n Puntatore del nodo da anteporre che deve essere scollegato
 *  @param      *t Puntatore ad un nodo che può essere concatenato ad altri nodi
 *  @return     enum e_node_err
 *  @code
 *  t_node *n = NULL;
 *  t_node *m = NULL;
 *  node_create( &n );
 *  node_create( &m );
 *  node_prepend( n, m );
 *  @endcode
 */
enum e_node_err node_prepend( t_node *n, t_node *t ) {
  /* n
     s <-> t <-> u
     s <-> n <-> t <-> u
     Il nodo n viene inserito prima di t che è una lista esistente, pertanto il nodo n deve essere scollegato.
  */
  if ( n != NULL && t != NULL ) {
    // Validazioni
    if ( n ==  t )
      return n_err_same;
    if ( n->prev != NULL || n->next != NULL )
      return n_err_linked;

    n->prev       = t->prev;
    n->next       = t;
    if ( t->prev != NULL )
      t->prev->next = n;
    t->prev       = n;
  }
  return n_err_ok;
}

/** @brief      Concatenazione di nodi
 *  @details    Collega il nodo n come successivo di m che può essere un nodo o una concatenazione di nodi
 *  @param      *n Puntatore del nodo da accodare che deve essere scollegato
 *  @param      *t Puntatore ad un nodo che può essere concatenato ad altri nodi
 *  @return     enum e_node_err
 *  @code
 *  t_node *n = NULL;
 *  t_node *m = NULL;
 *  node_create( &n );
 *  node_create( &m );
 *  node_append( n, m );
 *  @endcode
 */
enum e_node_err node_append( t_node *n, t_node *t ) {
  /* n
     s <-> t <-> u
     s <-> t <-> n <-> u
     Il nodo n viene inserito dopo t che è una lista esistente, pertanto il nodo n deve essere scollegato.
  */
  if ( n != NULL && t != NULL ) {
    // Validazioni
    if ( n ==  t )
      return n_err_same;
    if ( n->prev != NULL || n->next != NULL )
      return n_err_linked;

    n->prev       = t;
    n->next       = t->next;
    if ( t->next != NULL )
      t->next->prev = n;
    t->next       = n;
  }
  return n_err_ok;
}

/** @brief      Visualizzazione del contenuto di un nodo
 *  @details    Se il parametro cb_dump_node == NULL visualizza informazioni generiche memorizzate nella struttura dati t_node. 
 *              Se != NULL, esegue la call back con parametro t_node. 
 *              In ogni caso non inserisce a capi nè prima nè dopo l'esecuzione, questi andranno gestiti tramie le stringhe prefix e posfix
 *  @param      *n Puntatore al nodo
 *  @param      cb_dump_node Funzione callback per rappresentare i dati contenuti in t_node e t_node->data
 *  @param      prefix Stringa da visualizzare in testa ad ogni riga
 *  @param      posfix Stringa da visualizzare in coda ad ogni riga
 *  @code
 *  t_node *n = NULL;
 *  void dump( void *d ) { 
 *    t_data *td = ( t_data * )d; 
 *    printf("...", d ); 
 *  };
 *  node_create( &n );
 *  node_dump( n, dump, "\n-->", NULL );
 *  @endcode
 */
void node_dump( t_node *n, t_dump_node cb_dump_node, char *prefix, char *posfix ) {
  // Stringa prefix
  if ( prefix != NULL )
    printf( "%s", prefix );
  // Dump t_node
  if ( cb_dump_node != NULL ) {
      cb_dump_node( n );
  } else {
    printf( "Node  : ( %16p )", n );
    if ( n != NULL ) {
      printf( "  data: %16p prev: %16p next: %16p : ", n->data, n->prev, n->next );
    }
  }
  // Stringa posfix
  if ( posfix != NULL )
    printf( "%s", posfix );
}

/** @brief      Distruzione di un nodo
 *  @details    Se il nodo non trasportaa dati, lo scollega dalla lista, libera la memoria ed imposta il puntatore a NULL
 *  @param      **n Indirizzo del puntatore al nodo da distruggere
 *  @return     t_node **
 *  @return     e_node_err Informazioni sull'errore generato
 *  @code
 *  t_node*         n = NULL;
 *  enum e_node_err e;
 *  node_create( &n );
 *  e = node_destroy( &n );
 *  @endcode
 */
enum e_node_err node_destroy( t_node **n ) {
  /*
  Return code:
   0	: OK
   1	: Ci sono dati collegati
  */

  if ( *n != NULL ) {
    // Test che non ci siano dati puntati da questo nodo
    if ( (t_node **)(*n)->data != NULL )
      return  n_err_data;
    node_unlink( *n );
    cfree( *n );
    *n = NULL;
  }
  return n_err_ok;
}

/** @brief      Ricerca nodi
 *  @details    Se il nodo è concatenato ad altri nodi, restituisce il primo di tutta la catena. Se la catena è circolare restituisce quello successivo.
 *  @param      *n Puntatore al nodo
 *  @return     t_node **
 *  @code
 *  t_node*         n = NULL;
 *  ... altre dichiarazioni di nodi ...
 *  t_node*         m = NULL;
 *  node_create( &n );
 *  ... altre creazioni di nodi e loro concatenazioni ...
 *  m = node_get_first( n );
 *  @endcode
 */
t_node* node_get_first( t_node *n ) {
  t_node *test = NULL;
  t_node *curr = NULL;
  if ( n != NULL ) {
    test = n;	// evito loop in liste circolari
    if ( n->prev != NULL ) {
      for ( curr = n; curr->prev != NULL && curr->prev != test; curr = curr->prev )
        ;
    } else {
      curr = n;
    }
  }
  return curr;
}

/** @brief      Ricerca nodi
 *  @details    Se il nodo è concatenato ad altri nodi, restituisce l'ultimo di tutta la catena. Se la catena è circolare restituisce quello precedente.
 *  @param      *n Puntatore al nodo
 *  @return     t_node **
 *  @code
 *  t_node*         n = NULL;
 *  ... altre dichiarazioni di nodi ...
 *  t_node*         m = NULL;
 *  node_create( &n );
 *  ... altre creazioni di nodi e loro concatenazioni ...
 *  m = node_get_last( n );
 *  @endcode
 */
t_node* node_get_last( t_node *n ) {
  t_node *test = NULL;
  t_node *curr = NULL;
  if ( n != NULL ) {
    test = n;	// evito loop in liste circolari
    if ( n->next != NULL ) {
      for ( curr = n; curr->next != NULL && curr->next != test; curr = curr->next )
        ;
    } else {
      curr = n;
    }
  }
  return curr;
}

/** @brief      Numero di nodi collegati
 *  @details    Se il nodo è concatenato ad altri nodi è solo, restituisce il numero di nodi presenti nella concatenazione.
 *  @param      *n Puntatore al nodo
 *  @return     unsigned int
 *  @code
 *  t_node*         n = NULL;
 *  ... altre dichiarazioni di nodi ...
 *  unsigned int    l = 0;
 *  node_create( &n );
 *  ... altre creazioni di nodi e loro concatenazioni ...
 *  l = node_get_length( n );
 *  @endcode
 */
unsigned int node_length( t_node *n) {
  unsigned int p = 1;
  t_node *test   = NULL;
  t_node *curr   = NULL;

  if ( n != NULL ) {
    if ( n->prev != NULL )
      n = node_get_first( n );	    // Cerco il primo dei possibili nodi collegati ad n
    test = n;	                	// Evito loop in liste circolari
    for ( curr = n; curr->next != NULL && curr->next != test; curr = curr->next )
      p++;
  }
  return p;  
}

//	List

/** @brief      Creazione di una lista vuota
 *  @details    Riserva la memoria per la struttura dati e restituisce il puntatore alla lista vuota
 *              Inizializza a NULL i puntatori contenuti nella lista
 *  @param      **l Indirizzo del puntatore della lista da creare
 *  @return     t_list **
 *  @code
 *  t_list *l = NULL;
 *  list_create( &l );
 *  @endcode
 */
void list_create( t_list** l ) {
  *l = ( t_list * ) calloc( 1, sizeof( t_list ) );
  if ( *l != NULL ) {
    (( t_list *)(*l))->nodes = 0;
    (( t_list *)(*l))->data  = NULL;
    (( t_list *)(*l))->first = NULL;
    (( t_list *)(*l))->last  = NULL;
  }
}

/** @brief      Visualizzazione del contenuto di una lista
 *  @details    Se il parametro cb_dump_list == NULL visualizza informazioni generiche memorizzate nella struttura dati t_list. 
 *              Se != NULL, esegue la call back con parametro t_list. 
 *              Infine per ogni nodo esegue la node_dump passando il parametro cb_dump_node. 
 *              In ogni caso non inserisce a capi nè prima nè dopo l'esecuzione, questi andranno gestiti tramie le stringhe prefix e posfix
 *  @param      l Puntatore alla lista da esplorare
 *  @param      cb_dump_list Funzione callback per rappresentare i dati collegati all'header t_list
 *  @param      cb_dump_node Funzione callback per rappresentare i dati collegati ai nodi    t_node
 *  @param      prefix Stringa da visualizzare in testa ad ogni riga
 *  @param      posfix Stringa da visualizzare in coda ad ogni riga
 *  @code
 *  t_list *l = NULL;
 *  void dump( void *d ) { 
 *    t_data *td = ( t_data * )d; 
 *    printf("...", d ); 
 *  };
 *  list_create( &l );
 *  list_dump( l, NULL, dump, "\n-->", NULL );
 *  @endcode
 */
void list_dump( t_list *l, t_dump_list cb_dump_list, t_dump_node cb_dump_node, char *prefix, char *posfix ) {
  t_node *n = NULL;
  // Stringa prefix
  if ( prefix != NULL )
    printf( "%s", prefix );
  // Dump t_list
  if ( cb_dump_list != NULL ) {
    cb_dump_list( l );
  } else {
    printf( "List  : ( %16p )", l );
    if ( l != NULL ) {
      printf( "  nodes: %6d data: %16p first: %16p last: %16p ", l->nodes, l->data, l->first, l->last );
    }
  }
  // Scansione e dump di tutti i nodi
  for ( n = l->first; n != NULL; n = n->next ) {
    node_dump( n, cb_dump_node, prefix, posfix );
  }
  // Stringa posfix
  if ( posfix != NULL )
    printf( "%s", posfix );
}


/** @brief      Inserimento di un nodo in una lista
 *  @details    Inserisce un nodo ( anche collegato ad altri nodi ) nella lista ed aggiorna i dettagli nell'header, numero di nodi, primo e ultimo nodo.
 *  @param      *l  Puntatore alla lista
 *  @param      *n  Puntatore al nodo da inserire
 *  @param      pos Posizione in cui inserire il nodo. 0: Cima della lista, -1: Coda della lista
 *  @code
 *  t_list *l = NULL;
 *  list_create( &l );
 *  node_create( &n );
 *  list_insert( l, n, -1 );
 *  @endcode
 */
void list_insert( t_list *l, t_node *n, int pos ) {
  t_node *m  = NULL;
  t_node *nf = NULL;
  t_node *nl = NULL;
  int    len = 0;

  if ( l != NULL && n != NULL ) {

    if ( pos >= l->nodes )
      pos = -1;

    nf  = node_get_first( n );	// Se ci sono nodi collegati cerco il primo
    nl  = node_get_last( n );   // Se ci sono nodi collegati cerco l'ultimo
    len = node_length( nf );    // Salvo la lunghezza della lista di nodi collegatii a n

    if ( l->first == NULL && l->last == NULL ) {
      // Inserimento in una lista vuota
      l->first = nf;
      l->last  = nl;
    } else {
      // Inserimento in una lista popolata
      switch ( pos ) {
        case 0:				
          // Inserimento in prima posizione
          nl->next       = l->first;    // Collego l'ultimo nodo della 'lista' n al primo di l
          if ( l->first != NULL )
            l->first->prev = nl;        // Aggiorno il riferimento del nodo precedente del primo nodi di l all' ultimo nodo della 'lista' n
          l->first       = nf;          // Aggiorno il riferimento del primo nodo di l al primo nodo della 'lista' n
          break;
        case -1:			
          // Inserimento in ultima posizione
          nf->prev      = l->last;      // Collego il primo nodo della 'lista' n all' ultimo di l
          if ( l->last != NULL )
            l->last->next = nf;         // Aggiorno il riferimento del nodo successivo dell'ultimo dei nodi di l al primo nodo della 'lista' n
          l->last       = nl;           // Aggiorno il riferimento dell' ultimo nodo di l all'ultimo nodo della 'lista' n
          break;
        default:
          /*
             Inserimento in pos-esima posizione
             L:
             N0-m
             N0-n-m
          */
          m = l->first;
          while ( pos > 0 ) {
            m = m->next;
            pos--;
          }
          node_prepend( nl, m );
          break;
      }
    }
    // update numero nodi
    l->nodes += len;

  }
}


/** @brief      Eliminazione di un nodo dalla lista
 *  @details    Rimuove un nodo dalla lista liberando la memoria, assegnando NULL al puntatore ed aggiorna i dettagli nell'header della lista, numero di nodi, primo e ultimo nodo.
 *  @param      *l  Puntatore alla lista
 *  @param      **n Indirizzo del puntatore al nodo da inserire
 *  @returns    t_list **
 *  @returns    enum e_list_err
 *  @code
 *  t_list         *l = NULL;
 *  enum e_list_err e;
 *  list_create( &l );
 *  node_create( &n );
 *  e = list_node_destroy( l, &n );
 *  @endcode
 */
enum e_list_err list_node_destroy( t_list *l, t_node **n ) {
  /*
  Return code:
   0	: OK
   1	: Ci sono dati collegati al nodo da eliminare
  */
  char    ret      = 0;
  t_node  *n_first = NULL;
  t_node  *n_last  = NULL;
  bool    b_first  = false;
  bool    b_last   = false;

  if ( l != NULL && *n != NULL ) {
    // Nel caso il nodo da eliminare fosse il primo o l'ultimo salvo i riferimenti dei nodi collegati per aggiornare i dati della lista
    if ( *n == l->first ) {
      b_first = true;
      n_first = (t_node *)(*n)->next;
    }
    if ( *n == l->last ) {
      b_last  = true;
      n_last  = (t_node *)(*n)->prev;
    }

    ret = node_destroy( n );
    if ( ret != n_err_ok )
      return ret;

    // Se il nodo è stato eliminato aggiorno i riferimenti nella lista
    l->nodes--;
    if ( b_first )
      l->first = n_first;
    if ( b_last  )
      l->last  = n_last;
  }
  return n_err_ok;
}


/** @brief      Distruzione di una lista
 *  @details    Se la lista non trasportaa dati ed é vuota, libera la memoria ed imposta il puntatore a NULL
 *  @param      **l Indirizzo del puntatore alla lista da distruggere
 *  @return     t_list **
 *  @return     e_list_err Informazioni sul tipo di errore generato
 *  @code
 *  t_node*         n = NULL;
 *  enum e_list_err e;
 *  node_create( &n );
 *  e = node_destroy( &n );
 *  @endcode
 */
enum e_list_err list_destroy( t_list **l ) {
  /*
  Return code:
   0	: OK
   1	: Ci sono dati collegati alla lista
   2    : Ci sono nodi collegati alla lista
  */
  char    ret     = l_err_ok;

  if ( *l != NULL ) {
    // Nel caso ci siano dati collegati alla lista setto il flag di errore
    if ( (( t_list *)l)->data != NULL )
      ret |= l_err_data;
    // Nel caso ci siano nodi collegati alla lista setto il flag
    if ( (( t_list *)l)->first != NULL || (( t_list *)l)->last != NULL )
      ret |= l_err_node;
    if ( ret != l_err_ok )
      return ret;

    cfree( *l );
    *l = NULL;
  }
  return l_err_ok;
}


/** @brief      Aggiorna i puntatori al primo e all'ultimo nodo della lista
 *  @param      *l Puntatore alla lista
 *  @code
 *  t_list *l = NULL;
 *  list_create( &l );
 *  ... Inserimento, eliminazione, spostamento nodi ...
 *  list_update_bounds( l );
 *  @endcode
 */
void list_update_bounds( t_list *l ) {
  if ( l != NULL ) {
    if ( l->first != NULL )
      l->first = node_get_first( l->first );
    if ( l->last != NULL )
      l->last  = node_get_last( l->last );
  }
}
