#ifndef _LIST

#define _LIST

/**
 *
 *  @file       list.h
 *  @brief      Liste
 *  @author     Stefano Busnelli busnelli.stefano@gmail.com
 *  @version    1.3.0
 *  @details    Implementazione di una struttura dati di tipo lista non orientata.
 *      La struttura dati prevede un header di tipo t_list che contiene questi dati:
 *        * nodes:    Numero di nodi collegati.
 *        * data:    Puntatore ad un generico tipo di dati che conterrà valori comuni e di interesse generale per tutta la lista.
 *        * first:    Puntatore al primo nodo della lista.
 *        * last    Puntatore all' ultimo nodo della lista ( utile per accodare velocemente nuovi nodi ).
 *      Ogni nodo è una struttura dati di tipo t_node che contiene questi dati:
 *        * data:    Puntatore ad un generico tipo di dati che conterrà i valori memorizzati nel nodo.
 *        * next:    Puntatore al nodo successivo della lista.
 *        * prev:    Puntatore al nodo precedente della lista.
 * 
 *  Viene compilato come shared library
 * 
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

//  Enum
enum e_node_err { 
  n_err_ok,     ///<  Tutto OK 
  n_err_data,   ///<  Dati presenti 
  n_err_linked, ///<  Nodi collegati
  n_err_same    ///<  Stesso nodo
};
enum e_list_err { 
  l_err_ok,     ///<  Tutto OK
  l_err_data,   ///<  Dati presenti
  l_err_node    ///<  Nodi collegati
};

//    Definizione tipi struct

typedef struct t_node t_node;
typedef struct t_list t_list;

/** t_node
 *  Nodo della lista.
 */
typedef struct t_node {
  void   *data;             ///<  Puntatore ad un tipo di dato generico da memorizzare nel nodo della lista 
  t_node *next;             ///<  Nodo successivo della lista 
  t_node *prev;             ///<  Nodo precedente della lista 
} tt_node;

/** t_list
 *  Header della lista non orientata
 */
typedef struct t_list {
  unsigned int  nodes;      ///<  Numero di nodi contenuti nella lista
  void         *data;       ///<  Puntatore ad un tipo di dato generico che trasporta informazioni di carattere generale sulla lista
  t_node       *first;      ///<  Puntatore al primo nodo della lista
  t_node       *last;       ///<  Puntatore all' ultimo nodo della lista
} tt_list;

//    Prototipi funzioni

/** Prototipi di funzione callback usate per rappresentare i dati memorizzati nell'header e nei nodi
 */
typedef void ( *t_dump_list ) ( t_list* );
typedef void ( *t_dump_node ) ( t_node* );

//    Node

void            node_create( t_node **n );
void            node_unlink( t_node *n );
enum e_node_err node_prepend( t_node *n, t_node *t );
enum e_node_err node_append( t_node *n, t_node *t );
void            node_dump( t_node *n, t_dump_node cb_dump_node, char *prefix, char *posfix );
enum e_node_err node_destroy( t_node **n );
unsigned int 	node_length( t_node *n);
t_node*         node_get_first( t_node *n );
t_node*         node_get_last( t_node *n );

//    List

void            list_create( t_list** l );
void            list_dump( t_list *l, t_dump_list cb_dump_list, t_dump_node cb_dump_node, char *prefix, char *posfix );
void            list_insert( t_list *l, t_node *n, int pos );
enum e_list_err list_node_destroy( t_list *l, t_node **n );
enum e_list_err list_destroy( t_list **l );
void            list_update_bounds( t_list *l );

#endif
