# Liste #

Questa libreria implementa una struttura dati di tipo lista non orientata.

```
  ----------
  | t_list |
  |        |-----------------------------------------------------------------------------------
  |        |                                                                                  |
  ----------                                                                                  |
          |                                                                                   |
          v                                                                                   v
      ----------    ----------    ----------    ----------    ----------    ----------    ----------  
      | t_node |    | t_node |    | t_node |    | t_node |    | t_node |    | t_node |    | t_node |  
  ----|        |----|        |----|        |----|        |----|        |----|        |----|        |----
  |   |        |    |        |    |        |    |        |    |        |    |        |    |        |   | 
 -+-  ----------    ----------    ----------    ----------    ----------    ----------    ----------  -+-
  _                                                                                                    _
```

La struttura dati prevede un header di tipo t_list che contiene questi dati:
  * nodes:    Numero di nodi collegati.
  * data:    Puntatore ad un generico tipo di dati che conterrà valori comuni e di interesse generale per tutta la lista.
  * first:    Puntatore al primo nodo della lista.
  * last    Puntatore all' ultimo nodo della lista ( utile per accodare velocemente nuovi nodi ).
Ogni nodo è una struttura dati di tipo t_node che contiene questi dati:
  * data:    Puntatore ad un generico tipo di dati che conterrà i valori memorizzati nel nodo.
  * next:    Puntatore al nodo successivo della lista.
  * prev:    Puntatore al nodo precedente della lista.

La libreria e l'header vengono istallati nelle cartelle lib e include della home dir dell'utente che effettua la compilazione
E' necessario inserire il percorso della cartella lib nella variable LD_LIBRARY_PATH:

```
echo "export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:$HOME/lib" >> $HOME/.profile
source $HOME/.profile
```

## Compilazione ##

```
make all
```

## Install ##

```
make install
```

## Test ##

```
make test
cd bin
./node_create
```

## Utilizzo ###

main.c:
```
#include "list.h"

int main( char *argc, char **argv[] ) {
  t_node *n = NULL;
  node_create( &n );
  return 0;
}
```

### Compilazione: ###

```
gcc -o application -I$HOME/include -L$HOME/lib -llist main.c
```

### Esecuzione: ###

Il path dove risiede la libreria deve essere presente nella lista $LD_LIBRARY_PATH

```
./application
```
