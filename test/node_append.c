#include <stdio.h>
#include "../list.h"

void dump_data( t_node *node ) {
  if ( node != NULL && node->data != NULL )
    printf( "    Dato: %s", (char *) node->data );
}

int main( int argc, char **argv ) {
  t_node *n = NULL;
  t_node *m = NULL;

  printf( "\nCreo nodo N" );
  node_create( &n );
  n->data = "Nodo N";

  node_dump( n, NULL, "\n >", NULL );
  node_dump( n, dump_data, "\n :", NULL );

  printf( "\nCreo nodo M" );
  node_create( &m );
  m->data = "Nodo M";

  node_dump( m, dump_data, "\n :", NULL );

  printf( "\nAppendo N ad M" );
  if ( node_append( n, m ) != n_err_ok ) {
    printf( "\n### Non posso collegare i nodi ###\n" );
    return 1;
  }

  node_dump( n, dump_data, "\n :", NULL );
  node_dump( m, dump_data, "\n :", NULL );

  printf( "\nOK\n" );
 
  return 0;
}
