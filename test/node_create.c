#include <stdio.h>
#include "../list.h"

void dump_data( t_node *node ) {
  if ( node != NULL && node->data != NULL )
    printf( "    Dato: %s\n", (char *) node->data );
}

int main( int argc, char **argv ) {

  t_node *n = NULL;

  printf( "\nCreo nodo N\n" );
  node_create( &n );
  n->data = ( char * ) "Nodo N";
  node_dump( n, dump_data, "\n", NULL );

  printf( "\nOk.\n" );

  return 0;
}
