#include <stdio.h>
#include "../list.h"

void dump_data( t_node *node ) {
  if ( node != NULL && node->data != NULL )
    printf( "    Dato: %s\n", (char *) node->data );
}

int main( int argc, char **argv ) {

  t_node *n = NULL;
  t_node *m = NULL;

  printf( "\nCreo nodo N\n" );
  node_create( &n );
  n->data = ( char * ) "Nodo N";
  node_dump( n, dump_data, "\n", NULL );

  printf( "\nElimino nodo N: %p\n", n );
  if ( node_destroy( &n ) != n_err_ok ) {
    printf( "Non posso eliminare il nodo N %p\n", n );
  } else {
    printf( "Nodo %p\n", n );
  }

  printf( "\nCreo nodo M\n" );
  node_create( &m );
  node_dump( m, dump_data, "\n", NULL );

  printf( "\nElimino nodo M: %p\n", m );
  if ( node_destroy( &m ) != n_err_ok ) {
    printf( "Non posso eliminare il nodo M %p\n", m );
  } else {
    printf( "Nodo %p\n", m );
  }

  printf( "\nOk.\n" );

  return 0;
}
