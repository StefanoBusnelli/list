#include <stdio.h>
#include "../list.h"

void dump_data( t_node *node ) {
  if ( node != NULL && node->data != NULL )
    printf( "    Dato: %s", (char *) node->data );
}

int main( int argc, char **argv ) {
  t_node *n = NULL;
  t_node *m = NULL;
  t_node *o = NULL;

  printf( "\nCreo nodo N" );
  node_create( &n );
  n->data = "Nodo N";

  node_dump( n, dump_data, "\n  ", NULL );

  printf( "\n---   Last: %16p", node_get_last( n ) );

  printf( "\n--- Collego altri nodi ---" );
  
  printf( "\nCreo nodo M" );
  node_create( &m );
  m->data = "Nodo M";

  node_dump( m, dump_data, "\n  ", NULL );
  
  printf( "\nCreo nodo O" );
  node_create( &o );
  o->data = "Nodo O";

  node_dump( o, dump_data, "\n  ", NULL );

  printf( "\nAntepongo M ad N" );
  if( node_prepend( m, n ) != n_err_ok ) {
    printf( "\n### Non posso collegare i nodi ###\n" );
    return 1;
  }

  node_dump( m, dump_data, "\n  ", NULL );
  node_dump( n, dump_data, "\n  ", NULL );

  printf( "\nAppendo O ad N" );
  if( node_append( o, n ) != n_err_ok ) {
    printf( "\n### Non posso collegare i nodi ###\n" );
    return 1;
  }

  node_dump( n, dump_data, "\n  ", NULL );
  node_dump( o, dump_data, "\n  ", NULL );

  printf( "\n--- Situazione M N O ---" );

  node_dump( m, dump_data, "\n  ", NULL );
  node_dump( n, dump_data, "\n  ", NULL );
  node_dump( o, dump_data, "\n  ", NULL );

  printf( "\n--- Cerco Ultimo ---" );

  printf( "\n---   Last M: %16p", node_get_last( m ) );
  printf( "\n---   Last N: %16p", node_get_last( n ) );
  printf( "\n---   Last O: %16p", node_get_last( o ) );

  printf( "\nRendo la lista circolare" );

  m->prev = o;
  o->next = m;

  node_dump( m, dump_data, "\n  ", NULL );
  node_dump( n, dump_data, "\n  ", NULL );
  node_dump( o, dump_data, "\n  ", NULL );

  printf( "\n--- Cerco Ultimo lista circolare ---" );

  printf( "\n---   Last M: %16p", node_get_last( m ) );
  printf( "\n---   Last N: %16p", node_get_last( n ) );
  printf( "\n---   Last O: %16p", node_get_last( o ) );

  printf( "\nOK.\n" );

  return 0;
}
